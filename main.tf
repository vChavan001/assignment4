provider "aws" {
  region = "us-east-1"
}

# EC2 Instance
resource "aws_instance" "puppetNode" {
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  key_name      = "myec2key"

  tags = {
    Name = "puppetNode"
  }


# Provisioning script
provisioner "remote-exec" {
  inline = [
    "wget https://apt.puppetlabs.com/puppet8-release-jammy.deb",
    "sudo dpkg -i puppet8-release-jammy.deb",
    "sudo apt update",
    "sudo apt install puppet-agent"
  ]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("myec2key.pem") 
    host        = aws_instance.puppetNode.public_ip
  }
}
}